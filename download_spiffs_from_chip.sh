echo "Burning SPIFFS image..."
esptool.py --chip esp32 --port "/dev/ttyUSB0" --baud 1500000 --before default_reset --after hard_reset read_flash 0x00290000 1507328 filesystem/spiffs_download.bin

# 0x1000   bootloader_dio_40m.bin
# 0x8000   partitions.bin
# 0xe000   boot_app0.bin
# 0x10000  firmware.bin
# 0x290000 SPIFFS
