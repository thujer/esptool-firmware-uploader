# Lightweight ESP32 / ESP8266 firmware upload via ESPTool

Tool to help to rescue faraway Espressif project. Good helper to resuscite damaged firmware or filesystem.

## How to install Raspberry Image

Install Raspberry Pi Imager from https://www.raspberrypi.org/software/

![preview](https://gitlab.com/thujer/esptool-firmware-uploader/-/raw/master/docs/rpi-imager.jpg)


## How to setup wifi via CLI

Open the wpa-supplicant configuration file in nano:

```sudo nano /etc/wpa_supplicant/wpa_supplicant.conf```

Go to the bottom of the file and add the following:
```
network={
    ssid="Wifi1"
    psk="Password1"
}

network={
    ssid="Wifi2"
    psk="Password2"
}
```

The password can be configured either as the ASCII representation, in quotes as per the example above, or as a pre-encrypted 32 byte hexadecimal number. You can use the wpa_passphrase utility to generate an encrypted PSK. This takes the SSID and the password, and generates the encrypted PSK. With the example from above, you can generate the PSK with wpa_passphrase "testing". Then you will be asked for the password of the wireless network (in this case testingPassword). The output is as follows:

```
network={
      ssid="testing"
      #psk="testingPassword"
      psk=131e1e221f6e06e3911a2d11ff2fac9182665c004de85300f9cac208a6a80531
}
```

More info at https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md


## How to install ESPTOOL

You will need either Python 2.7 or Python 3.4 or newer installed on your system.

The latest stable esptool.py release can be installed from pypi via pip:

```
$ pip install esptool
```

Source: https://github.com/espressif/esptool

## How to make binary partition

Find esp32 tools
/.platformio/packages/framework-arduinoespressif32/tools

Example partition generate
python gen_esp32part.py --verify partitions/large_spiffs_16MB.csv large_spiffs_16MB.bin

More info at
https://demo-dijiudu.readthedocs.io/en/latest/api-guides/partition-tables.html#creating-custom-tables


## How to minitor serial port

Install Minicom by
```
sudo apt install minicom
```

To monitor port just run
```
minicom -D /dev/ttyUSB0 -b 115200
```


---
## Links

https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md

https://blog.vyoralek.cz/iot/esp8266-a-esp32-zaloha-a-nahrani-noveho-firmware-pomoci-esptool/

https://nodemcu.readthedocs.io/en/latest/flash/

http://domoticx.com/esp8266-esptool-bootloader-communicatie/

https://docs.google.com/document/d/1rpTHDIMSTLdcPiox6WUEr17HxQI-9zkjTcpU9YlX8e4/edit#heading=h.ne5kk7dciayh

https://community.platformio.org/t/export-of-binary-firmware-files-for-esp32-download-tool/9253

https://esp32.com/viewtopic.php?t=8539

https://www.cyberciti.biz/hardware/5-linux-unix-commands-for-connecting-to-the-serial-console/

