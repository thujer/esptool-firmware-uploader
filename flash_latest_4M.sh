echo "Downloading latest..."
wget repo.spsy.eu/projects/rfid-m5stack/firmware/latest/firmware.bin -O firmware/latest.bin

#echo "Checking hardware..."
#esptool.py --port /dev/ttyUSB0 flash_id

echo "Burning firmware..."
esptool.py --chip esp32 --port "/dev/ttyUSB0" --baud 1500000 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 40m --flash_size detect 0x1000 bootloaders/bootloader_dio_40m.bin 0x8000 partitions/default.bin 0xe000 bootloaders/boot_app0.bin 0x10000 firmware/latest.bin

# 0x1000 bootloader_dio_40m.bin
# 0x8000 partitions.bin
# 0xe000 boot_app0.bin
# 0x10000 firmware.bin

