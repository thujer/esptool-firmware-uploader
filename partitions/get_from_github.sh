#!/bin/sh

if [ -z "$1" ]
then
      echo "\nFirst param must be a file (partition) name from https://github.com/espressif/arduino-esp32/tree/master/tools/partitions"
else
      wget https://raw.githubusercontent.com/espressif/arduino-esp32/master/tools/partitions/$1
fi
