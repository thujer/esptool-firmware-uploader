#!/bin/bash

# 0x1000 bootloader_dio_40m.bin
# 0x8000 partitions.bin
# 0xe000 boot_app0.bin
# 0x10000 firmware.bin


# Flash params set to 0x0240

# Bootloader
# Compressed 15872 bytes to 10319...
# Writing at 0x00001000... (100 %)
# Wrote 15872 bytes (10319 compressed) at 0x00001000 in 0.1 seconds (effective 1306.8 kbit/s)...

# Partition
# Compressed 3072 bytes to 129...
# Writing at 0x00008000... (100 %)
# Wrote 3072 bytes (129 compressed) at 0x00008000 in 0.0 seconds (effective 11444.3 kbit/s)...

# Boot APP0
# Compressed 8192 bytes to 47...
# Writing at 0x0000e000... (100 %)
# Wrote 8192 bytes (47 compressed) at 0x0000e000 in 0.0 seconds (effective 31465.0 kbit/s)...

# Firmware
# Compressed 1280192 bytes to 732148...
# Wrote 1280192 bytes (732148 compressed) at 0x00010000 in 10.2 seconds (effective 1008.3 kbit/s)...

# SPIFFS
# Compressed 3604480 bytes to 287869...
# Wrote 3604480 bytes (287869 compressed) at 0x00c90000 in 4.2 seconds (effective 6943.9 kbit/s)...

unset options i

while IFS= read -r -d $'\0' f; do
  options[i++]="$f"
done < <(find partitions/ -maxdepth 1 -type f -name "*.csv" -print0 )

select opt in "Download original partition" "${options[@]}" "Stop the script"; do
    case $opt in
        *.csv)

        LINE=`cat $opt | grep app0 | tr ',' ' '`
        ARR=($LINE)
        APP_OFFSET=${ARR[3]//,}
        SELECTED_PARTITION=${opt//.csv}

        LINE_SPIFFS=`cat $opt | grep spiffs | tr ',' ' '`
        ARR_SPIFFS=($LINE_SPIFFS)
        SPIFFS_OFFSET=${ARR_SPIFFS[3]//,}

        echo ""
        echo "Summary:"
        echo "CSV file $opt selected"
        echo "Detected APP0 offset $APP_OFFSET"
        echo "Detected SPIFFS offset $SPIFFS_OFFSET"
        echo "Selected partition $SELECTED_PARTITION"
        echo ""

        read -r -p "Are you sure to flash APP0? [y/N] " response
        case "$response" in
            [yY][eE][sS]|[yY]) 

                #echo "Checking hardware..."
                #esptool.py --port /dev/ttyUSB0 flash_id

                echo "Downloading latest..."
                wget repo.spsy.eu/projects/rfid-m5stack/firmware/latest/firmware.bin -O firmware/latest.bin

                echo "Burning firmware..."
                esptool.py --chip esp32 --port "/dev/ttyUSB0" --baud 1500000 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 40m --flash_size detect 0x1000 bootloaders/bootloader_dio_40m.bin 0x8000 $SELECTED_PARTITION.bin 0xe000 bootloaders/boot_app0.bin $APP_OFFSET firmware/latest.bin

                ;;
            *)
                ;;
        esac

        echo ""
        echo "Summary:"
        echo "CSV file $opt selected"
        echo "Detected SPIFFS offset $SPIFFS_OFFSET"
        echo "Selected partition $SELECTED_PARTITION"
        echo ""

        read -r -p "Are you sure to flash SPIFFS? [y/N] " response
        case "$response" in
            [yY][eE][sS]|[yY]) 

                echo "Downloading latest SPIFFS image ..."
                wget repo.spsy.eu/projects/rfid-m5stack/firmware/latest/spiffs.bin -O filesystem/spiffs.bin

                echo "Burning SPIFFS image..."
                esptool.py --chip esp32 --port "/dev/ttyUSB0" --baud 1500000 --before default_reset --after hard_reset write_flash -z $SPIFFS_OFFSET filesystem/spiffs.bin

                minicom -D /dev/ttyUSB0 -b 115200

                break;
                ;;
            *)
                break;
                ;;
        esac
        ;;

    "Stop the script")
        echo "You chose to stop"
        break
        ;;

    "Download original partition")
        echo "Downloading latest partition file ..."
        wget repo.spsy.eu/projects/rfid-m5stack/firmware/latest/partitions.bin -O partitions/partitions.bin

        echo "Downloading latest firmware ..."
        wget repo.spsy.eu/projects/rfid-m5stack/firmware/latest/firmware.bin -O firmware/latest.bin

        echo "Flashing APP..."
        esptool.py --chip esp32 --port "/dev/ttyUSB0" --baud 1500000 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 40m --flash_size detect 0x1000 bootloaders/bootloader_dio_40m.bin 0x8000 partitions/partitions.bin 0xe000 bootloaders/boot_app0.bin 0x10000 firmware/latest.bin

        unset options i

        while IFS= read -r -d $'\0' f; do
            options[i++]="$f"
        done < <(find partitions/ -maxdepth 1 -type f -name "*.csv" -print0 )

        select opt in "${options[@]}"; do
            case $opt in
                *.csv)

                LINE_SPIFFS=`cat $opt | grep spiffs | tr ',' ' '`
                ARR_SPIFFS=($LINE_SPIFFS)
                SPIFFS_OFFSET=${ARR_SPIFFS[3]//,}

                echo ""
                echo "ARE YOU SURE WITH:"
                echo "CSV file $opt selected"
                echo "DETECTED SPIFFS OFFSET: $SPIFFS_OFFSET"
                echo ""

                read -r -p "Are you sure to flash SPIFFS? [y/N] " response
                case "$response" in
                    [yY][eE][sS]|[yY]) 

                        echo "Downloading latest SPIFFS image ..."
                        wget repo.spsy.eu/projects/rfid-m5stack/firmware/latest/spiffs.bin -O filesystem/spiffs.bin

                        echo "Burning SPIFFS image..."
                        esptool.py --chip esp32 --port "/dev/ttyUSB0" --baud 1500000 --before default_reset --after hard_reset write_flash -z $SPIFFS_OFFSET filesystem/spiffs.bin

                        minicom -D /dev/ttyUSB0 -b 115200

                        break;

                        ;;
                    *)
                        break;
                        ;;
                esac
            esac
            done
        break
        ;;

    *)
      echo "This is not a number"
      ;;
  esac
done
