
# 0x1000   bootloader_dio_40m.bin
# 0x8000   partitions.bin
# 0xe000   boot_app0.bin
# 0x10000  firmware.bin
# 0x290000 SPIFFS ... Offset depends on selected partition !!!

#!/bin/bash

unset options i

while IFS= read -r -d $'\0' f; do
  options[i++]="$f"
done < <(find partitions/ -maxdepth 1 -type f -name "*.csv" -print0 )

select opt in "${options[@]}" "Stop the script"; do
  case $opt in
    *.csv)
      
      LINE=`cat $opt | grep spiffs | tr ',' ' '`
      ARR=($LINE)
      SPIFFS_OFFSET=${ARR[3]//,}
      SELECTED_PARTITION=${opt//.csv}

      echo "Summary:"
      echo "CSV file $opt selected"
      echo "Detected SPIFFS offset $SPIFFS_OFFSET"
      echo "Selected partition $SELECTED_PARTITION"
      echo ""

      read -r -p "Are you sure to flash SPIFFS? [y/N] " response
      case "$response" in
          [yY][eE][sS]|[yY]) 

            echo "Downloading latest SPIFFS image ..."
            wget repo.spsy.eu/projects/rfid-m5stack/firmware/latest/spiffs.bin -O filesystem/spiffs.bin

            echo "Burning SPIFFS image..."
            esptool.py --chip esp32 --port "/dev/ttyUSB0" --baud 1500000 --before default_reset --after hard_reset write_flash -z $SPIFFS_OFFSET filesystem/spiffs.bin

            break;

            ;;
          *)
            break;
            ;;
      esac

      ;;
    "Stop the script")
      echo "You chose to stop"
      break
      ;;
    *)
      echo "This is not a number"
      ;;
  esac
done
